db.inventory.insertMany([
		{
			"name": "Javascript for beginners",
			"author": "James Doe",
			"price": 5000,
			"stocks": 50,
			"publisher": "JS Publishing House"
		},
		{
			"name": "HTML and CSS",
			"author": "John Thomas",
			"price": 2500,
			"stocks": 38,
			"publisher": "NY Publishers"
		},
		{
			"name": "Web Development Fundamentals",
			"author": "Noah Jimenez",
			"price": 3000,
			"stocks": 10,
			"publisher": "Big Idea Publishing"
		},
		{
			"name": "Java Programming",
			"author": "David Michael",
			"price": 10000,
			"stocks": 100,
			"publisher": "JS Publishing House"
		}


	])

// ---------------------------------------------------------------
// COMPARISON QUERY OPERATORS
/*
		$gt/$gte oprator (greater than/ greater than or equal)

			syntax:
				db.collection.find({"field": { $gt:value }})
				db.collection.find({"field": { $gte:value }})


*/

db.inventory.find({
	"stocks": {
		$gt: 50
	}
})

db.inventory.find({
	"stocks": {
		$gte: 50
	}
})



// less tham operator ------------------------------------------
/*
	syntax:
		db.collection.find({ "field": { $lt: value}})
		db.collection.find({ "field": { $lte: value}})

*/

db.inventory.find({
	"stocks": {
		$lt: 50
	}
})

db.inventory.find({
	"stocks": {
		$lte: 50
	}
})

// not equal operator-----------------------------------------------------
/*
	syntax:
		db.collectionName.find({ "field": { $ne:value}})
*/

db.inventory.find({
	"stocks": {
		$ne: 50
	}
})

// equal operator--------------------------------------------------
/*
	syntax:
		db.collection.fing({ "field": {$eq: value}})

*/

db.inventory.find({
	"stocks": {
		$eq: 50
	}
})

// $in operator (OR operator)-----------------------------------------------
/*
	syntax:
		db.collection.find({ "field": {$in: [value1, value2] }})
*/

db.inventory.find({
	"price": {
		$in: [10000, 5000]
	}
})


// $nin operator (not $in operator/not equal)
/*
	syntax:
		db.collectionName.find({ "field": {$nin: [value1, value2] }})
*/

db.inventory.find({
	"price": {
		$nin: [10000, 5000]
	}
})


// ranged --------------------------------------------------------------

db.inventory.find ({
	"price": {$gt: 2000} && {$lt: 4000}
})


// mini activity---------------------------------------------------------

db.inventory.find ({
	"price": {$lte: 4000}
})

db.inventory.find({
	"stocks": {
		$in: [50, 100]
	}
})


// ------------------------------------------------------------
//LOGICAL QUERY OPERATORS 
/*	
	
	$or operator
	Syntax:
		db.collectionName.find({
			$or:[
				{"fieldA": valueA},
				{"fieldB": valueB}
			]
		})
*/

db.inventory.find({
	$or: [
		{"name": "HTML and CSS"},
		{"publisher": "JS Publishing House"}
	]
})

db.inventory.find({
	$or: [
		{"author": "James Doe"},
		{"price": {$lte: 5000}}
	]
})


// and operator----------------------------------------------------------
/*
	$and operator
		syntax:
			db.collectionName.find({ $and: [ {"fieldA": valueA}, {"fieldB": valueB} ] })
*/
db.inventory.find({
	$and: [
		{"stocks": {$ne: 50}},
		{"price": {$ne: 5000}}
	]
})

// by default same lang sila ng $and 
db.inventory.find({
	"stocks": {$ne: 50},
	"price": {$ne: 5000}
})

// --------------------------------------------------------------
// Field Projection
// Inclusion 
/*
	syntax:
		db.collectionName.find({criteria}, {"field": 1})

		lagyan ng 1 yung gustong iinclude. parang boolean. 1=true. si objectID lang pwede lagyan ng 0.
*/

db.inventory.find(
	{"publisher": "JS Publishing House"},
	{
		"name": 1,
		"author":1,
		"price": 1
	}

	)

// Exclusion---------------------------------------------------
/*
	Syntax:
		db.collection.find({criteria}, {field: 0})
*/

db.inventory.find(
		{"author": "Noah Jimenez"},
		{
			"price": 0,
			"stocks": 0
		}
	)

// hindi pwede pagsabayin ang exclusion and inclusion

db.inventory.find(
		{"price": {$lte: 5000}},
		{
			"_id":0,
			"name": 1,
			"author":1
		}
	)



// -------------------------------------------------------------------
// EVALUATION QUERY OPERATOR
// $regex operator
/*
	syntax:
		db.collectionName.find({field: {$regex: 'pattern', $options: 'value'}})
*/

// case sensitive
db.inventory.find({
	"author": {
		$regex: 'M',
	}
})


// case insensitive
db.inventory.find({
	"author": {
		$regex: 'M',
		$options: '$i'
	}
})



// can query operators be used on operation such as updateOne, updateMany, deleteOne, deleteMany??
/*
	YES

	updateOne({criteria/query}, {$set})
	updateMany({criteria/query}, {$set})
	deleteOne({criteria/query})
	deleteMany({criteria/query})
*/